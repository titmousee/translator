package com.titmouse.anton.translator.ui;

/**
 * Created by anton on 24.04.17.
 */

public interface  FragmentLifecycle {
     void onOpenFragment();
}
